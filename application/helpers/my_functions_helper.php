<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 * Date: 22/06/2018
 * Time: 12:16
 */
add_action('web_to_lead_form_submitted','my_web_to_lead_form_submitted_redirect_to_custom_url');

function my_web_to_lead_form_submitted_redirect_to_custom_url($data){
    echo json_encode(array(
        'success'=>true,
        'redirect_url'=>'https://www.agenciatresmeiazero.com.br/sites/obrigado/'
    ));
    die;
}